# Barcelona
### És una ciutat i metròpoli a la costa mediterrània de la península Ibèrica. És la capital de Catalunya, així com de la comarca del Barcelonès i de la província de Barcelona, i la segona ciutat en població i pes econòmic de la península Ibèrica, després de Madrid. 
___

## Llistat de barris (unordered *-+):
+ Sant Martí
+ Sant Andreu
+ Nou Barris

## Llistat de barris (ordered):
1. Gràcia
2. Ciutat Vella
3. Les Corts

![BarcelonaImage](/images/barcelonacity.jpg)